#ifndef PREVIEW_H__
#define PREVIEW_H__

#include "main.h"
#include "settings.h"
#include "scene.h"

class Preview
{
public:
  struct SlideColor
  {
    unsigned char r,g,b,is_primary;
    int dot;

    SlideColor (char r = 0, char g = 0, char b = 0, char is_primary = 1, int dot = 1)
    : r(r),g(g),b(b),is_primary(is_primary),dot(dot) {}
  };
  struct Slide
  {
    std::vector<SlideColor> colors;
    float time;
  };

  class ColorBarDrawing : Fl_Widget
  {
  private:
    static Slide default_slide;
    Slide& current_slide;

  public:
    ColorBarDrawing (int x, int y, int w, int h, const char *label = 0)
      : Fl_Widget(x,y,w,h,label),current_slide(default_slide)
    {
      current_slide.time = 0;
    }

    void redraw1 ()
    {
      redraw();
    }

    void SetCurrentSlide (Slide& slide = default_slide)
    {
      current_slide = slide;
      redraw();
    }

    void DrawSymbol (const SlideColor& slide, int x, int y, int r)
    {
      int avg = ((unsigned int)(slide.r) + (unsigned int)(slide.g) + (unsigned int)(slide.b)) / 3;
      fl_color(fl_rgb_color(255-avg,255-avg,255-avg));
      fl_line_style(FL_SOLID, r>>1);

      if ( slide.dot == DOT_BACK )
      {
        fl_line(x-r,y,x+r,y);
      }
      else if ( slide.dot == DOT_FORWARD )
      {
        fl_line(x-r,y,x+r,y);
        fl_line(x,y-r,x,y+r);
      }
    }

    virtual void draw ()
    {
      fl_color(fl_rgb_color(192,192,192));
      fl_rectf(x(),y(),w(),h());

      float length = 0.0f;
      unsigned char prev = 0xff;
      int black = 0, white = 0;
      std::vector<SlideColor>::const_iterator i;
      for (i = current_slide.colors.begin(); i != current_slide.colors.end(); i++)
      {
        if ( (*i).is_primary )
        {
          white++;
        }
        else
        {
          black++;
        }

        if ( (*i).is_primary == prev || length == 0 )
        {
          length += 1.0f;
        }
        else
        {
          length += 0.5f;
        }
        prev = (*i).is_primary;
      }

      float box_w = float(w()) / length;
      float symbol_w = float(h()) * 0.125f;
      float symobl_xoffset = box_w * 0.5f;

      float top_symbol_yoffset = float(h()) * 0.25f;
      float bottom_symbol_yoffset = ( !black ? float(h()) * 0.5f : float(h()) * 0.75f);

      float top_box_h = float(h()) * 0.5f;

      float pos;
      pos = 0.0f;
      prev = 0xff;
      for (i = current_slide.colors.begin(); i != current_slide.colors.end(); i++)
      {
        if ( i == current_slide.colors.begin() )
        {
        }
        else if ( (*i).is_primary == prev )
        {
          pos += 1.0f;
        }
        else
        {
          pos += 0.5f;
        }

        if ( (*i).is_primary )
        {
          float xpos = x() + pos * box_w;
          fl_color(fl_rgb_color((*i).r,(*i).g,(*i).b));
          fl_rectf(xpos, y(), box_w, ( (*i).is_primary ? h() : top_box_h ));
          DrawSymbol((*i), xpos + symobl_xoffset, y() + ( (*i).is_primary ? bottom_symbol_yoffset : top_symbol_yoffset ), symbol_w);
        }

        prev = (*i).is_primary;
      }
      pos = 0.0f;
      prev = 0xff;
      for (i = current_slide.colors.begin(); i != current_slide.colors.end(); i++)
      {
        if ( i == current_slide.colors.begin() )
        {
        }
        else if ( (*i).is_primary == prev )
        {
          pos += 1.0f;
        }
        else
        {
          pos += 0.5f;
        }

        if ( !(*i).is_primary )
        {
          float xpos = x() + pos * box_w;
          fl_color(fl_rgb_color((*i).r,(*i).g,(*i).b));
          fl_rectf(xpos, y(), box_w, ( (*i).is_primary ? h() : top_box_h ));
          DrawSymbol((*i), xpos + symobl_xoffset, y() + ( (*i).is_primary ? bottom_symbol_yoffset : top_symbol_yoffset ), symbol_w);
        }

        prev = (*i).is_primary;
      }

      return;
    }
  };

  bool playing;
  float play_position;
  int current_slide;
  float total_duration;

  std::vector<Settings::ColorValue>& colorvalues;
  std::vector<Scenes::SceneValue>& scenevalues;

  Fl_Group* group;
  ColorBarDrawing* colorbardrawing;
  ColorBarDrawing* win_colorbardrawing;
  Fl_Button* playbtn;
  Fl_Slider* timeline;
  Fl_Button* prev;
  Fl_Button* next;
  Fl_Button* reload;

public:
  std::vector<Slide> slides;

  Preview (std::vector<Settings::ColorValue>& cvs, std::vector<Scenes::SceneValue>& svs, ColorBarDrawing* win_colorbardrawing)
    : colorvalues(cvs),scenevalues(svs),win_colorbardrawing(win_colorbardrawing)
  {
    playing = false;
    total_duration = 0;

    Fl::visual(FL_RGB8);
    group = new Fl_Group(0, 50, 480, 295);
    group->label("Preview");
    group->labelsize(12);

    colorbardrawing = new ColorBarDrawing(10,60,460,220);

    playbtn = new Fl_Button(10,290,20,20,">");
    playbtn->callback(cb_play,this);

    timeline = new Fl_Slider(30,290,350,20);
    timeline->type(FL_HORIZONTAL);
    timeline->callback(cb_position,this);
    timeline->when(FL_WHEN_CHANGED);

    prev = new Fl_Button(380,290,20,20,"|<");
    prev->callback(cb_prev,this);

    next = new Fl_Button(400,290,20,20,">|");
    next->callback(cb_next,this);

    reload = new Fl_Button(420,290,50,20,"refresh");
    reload->labelsize(12);

    group->end();
  }

  void CompileSlides ()
  {
    playing = false;
    total_duration = 0.0f;

    int cs = int(colorvalues.size()) - 1;
    if ( cs < 0 ) return;

    std::vector<Scenes::SceneValue>::const_iterator i;
    std::vector<Settings::ColorValue>::const_iterator j;

    slides.clear();
    for (i = scenevalues.begin(); i != scenevalues.end(); i++)
    {
      Slide slide;

      int c, l;

      c = int((*i).position * float(cs+1));
      c = std::min(std::max(0,c),cs);

      l = c - (*i).left;
      l = std::min(std::max(0,l),cs);

      for (unsigned int j = 0; j < (*i).dots.size(); j++)
      {
        Settings::ColorValue& cv = colorvalues[l+j];
        slide.colors.push_back(SlideColor(cv.r,cv.g,cv.b,cv.is_primary,(*i).dots[j]));
      }

      total_duration += slide.time = (*i).duration;

      slides.push_back(slide);
    }

    current_slide = 0;

    if ( slides.size() )
    {
      colorbardrawing->SetCurrentSlide(slides[0]);
      win_colorbardrawing->SetCurrentSlide(slides[0]);
    }
    else
    {
      colorbardrawing->SetCurrentSlide();
      win_colorbardrawing->SetCurrentSlide();
    }
  }

  static void cb_position (Fl_Widget* a, void* data)
  {
    Preview* p = (Preview*)data;
    if ( !p  || !p->slides.size() ) return;

    p->playing = false;
    float v = p->total_duration * p->timeline->value();
    float current = 0;
    const int c = p->slides.size();
    for (int i = 0; i < c; i++)
    {
      current += p->slides[i].time;
      if ( v < current )
      {
        if ( p->current_slide != i )
        {
          p->current_slide = i;
          p->colorbardrawing->SetCurrentSlide(p->slides[p->current_slide]);
          p->win_colorbardrawing->SetCurrentSlide(p->slides[p->current_slide]);
        }
        return;
      }
    }
  }

  static void cb_next (Fl_Widget* a, void* data)
  {
    Preview* p = (Preview*)data;
    if ( !p  || !p->slides.size() || p->current_slide >= int(p->slides.size()) - 1 ) return;

    p->playing = false;

    p->play_position += p->slides[p->current_slide].time / p->total_duration;
    p->timeline->value(p->play_position);

    p->current_slide++;
    p->colorbardrawing->SetCurrentSlide(p->slides[p->current_slide]);
    p->win_colorbardrawing->SetCurrentSlide(p->slides[p->current_slide]);
  }

  static void cb_prev (Fl_Widget* a, void* data)
  {
    Preview* p = (Preview*)data;
    if ( !p  || !p->slides.size() || p->current_slide == 0 ) return;

    p->playing = false;

    p->current_slide--;
    p->colorbardrawing->SetCurrentSlide(p->slides[p->current_slide]);
    p->win_colorbardrawing->SetCurrentSlide(p->slides[p->current_slide]);

    p->play_position -= p->slides[p->current_slide].time / p->total_duration;
    p->timeline->value(p->play_position);
  }

  static void cb_play (Fl_Widget* a, void* data)
  {
    Preview* p = (Preview*)data;
    if ( !p ) return;

    if ( !p->playing )
    {
      if ( p->slides.size() )
      {
        p->playing = true;
        p->playbtn->label("X");
        p->current_slide = 0;
        p->play_position = 0.0f;
        cb_playback(data);
      }
    }
    else
    {
      p->playing = false;
      p->playbtn->label(">");
    }
  }

  static void cb_playnet (void* data)
  {
    cb_play(NULL,data);
  }

  static void cb_rewind (void* data)
  {
    Preview* p = (Preview*)data;
    if (!p ) return;

    Fl::remove_timeout(cb_playback);

    p->playing = false;
    p->playbtn->label(">");
    p->current_slide = 0;
    p->play_position = 0.0f;
    p->timeline->value(p->play_position);

    if ( p->slides.size() )
    {
      p->colorbardrawing->SetCurrentSlide(p->slides[p->current_slide]);
      p->win_colorbardrawing->SetCurrentSlide(p->slides[p->current_slide]);
      p->colorbardrawing->redraw1();
      p->win_colorbardrawing->redraw1();
    }
  }

  static void cb_playback (void* data)
  {
    Preview* p = (Preview*)data;

    if ( !p ) return;

    p->timeline->value(p->play_position);
    if ( p->play_position >= 1.0f ) p->playing = false;

    if ( !p->playing )
    {
      p->current_slide--;
      if ( p->current_slide < 0 ) p->current_slide = 0;
      p->playbtn->label(">");
      return;
    }

    printf("play!!!\n");

    p->colorbardrawing->SetCurrentSlide(p->slides[p->current_slide]);
    p->win_colorbardrawing->SetCurrentSlide(p->slides[p->current_slide]);

    float offset = p->slides[p->current_slide].time;
    p->play_position += offset / p->total_duration;
    if ( p->current_slide < int(p->slides.size()) )
    {
      printf("play!!! %f\n",offset);
      p->current_slide++;
      Fl::add_timeout(offset,cb_playback,data);
    }
    else
    {
    printf("denidedd!!!\n");
      p->playing = false;
      p->playbtn->label(">");
    }
  }
};
Preview::Slide Preview::ColorBarDrawing::default_slide;

#endif
