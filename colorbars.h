#ifndef COLORBARS_H__
#define COLORBARS_H__

#include "main.h"
#include "preview.h"
#include "scene.h"
#include "settings.h"

class ColorBars
{
public:
  class Display : public Fl_Window
  {
  private:
    int mx, my;
    ColorBars* cb;
    float play_position;
    bool playing;

  public:
    Display (int w, int h, ColorBars* cb)
      : Fl_Window(w,h), cb(cb) { border(false); }

    virtual int handle (int event)
    {
      // handle toggles...
      if ( event == FL_KEYBOARD )
      {
        if ( Fl::event_key() == ' ' )
        {
          for (unsigned int i = 0; i < cb->ports.size(); i++)
          {
            char num[32];
            snprintf(num,32,"%d",cb->ports[i]);
            printf("%s\n",num);
            lo_address t = lo_address_new(NULL,num);
            lo_send(t,"/play",NULL);
            lo_address_free(t);
          }
        }
        else if ( Fl::event_key() == FL_BackSpace )
        {
          for (unsigned int i = 0; i < cb->ports.size(); i++)
          {
            char num[32];
            snprintf(num,32,"%d",cb->ports[i]);
            lo_address t = lo_address_new(NULL,num);
            lo_send(t,"/rewind",NULL);
            lo_address_free(t);
          }
        }
        else if ( Fl::event_key() == FL_Escape )
        {
          cb->window->show();
          hide();
        }
      }
      else if ( event == FL_PUSH && Fl::event_button() == FL_LEFT_MOUSE )
      {
        cursor(FL_CURSOR_MOVE);
        mx = Fl::event_x();
        my = Fl::event_y();
      }
      else if ( event == FL_DRAG && Fl::event_state() & FL_BUTTON1 )
      {
        int dx = Fl::event_x() - mx;
        int dy = Fl::event_y() - my;
        resize(x()+dx,y()+dy,w(),h());
      }
      else if ( event == FL_PUSH && Fl::event_button() == FL_RIGHT_MOUSE )
      {
        cursor(FL_CURSOR_NWSE);
        mx = Fl::event_x();
        my = Fl::event_y();
      }
      else if ( event == FL_DRAG && Fl::event_state() & FL_BUTTON3 )
      {
        int dx = Fl::event_x() - mx;
        int dy = Fl::event_y() - my;
        mx = Fl::event_x();
        my = Fl::event_y();
        resize(x(),y(),w()+dx,h()+dy);
        if ( w() < 100 ) w(100);
        if ( h() < 100 ) h(100);
      }
      else if ( event == FL_RELEASE )
      {
        cursor(FL_CURSOR_DEFAULT);
      }
      return 1;
    }
  };

private:
  std::string filename;
  std::string window_name;

  bool is_modified;
  bool unsaved;
  std::ifstream ifile;

  bool play_window_border;
  Preview::ColorBarDrawing* play_window_drawing;

  Fl_Window* window;
  Fl_Menu_Bar* menu;
  Fl_Tabs* tabs;

public:
  Display* play_window;

  Settings* settings;
  Scenes* scenes;
  Preview* preview;
  std::vector<int> ports;

  static ColorBars* current;

  void setPorts (const std::vector<int>& ps)
  {
    ports = ps;
  }

  void settitle ()
  {
    char fn[128];
    fl_filename_relative(fn,sizeof(fn),filename.c_str());

    std::stringstream ss;
    ss << PROGRAM_NAME " - ";
    if ( is_modified ) ss << "*";
    ss << "[" << fn << "]";

    window_name = ss.str();
    window->label(window_name.c_str());
  }

  void modify ()
  {
    if ( !is_modified )
    {
      is_modified = true;
      settitle();
    }
  }

  void newfile ()
  {
    filename = "Untitled";
    is_modified = false;
    unsaved = true;

    settings->set(std::vector<Settings::ColorValue>(),0,1,0);
    scenes->set(std::vector<Scenes::SceneValue>(),0);

    settitle();
  }

  void open (const char* fn)
  {
    std::ifstream ifile(fn);
    if ( ifile.is_open() )
    {
      std::vector<Settings::ColorValue> colorvalues;
      int seed;
      int subcards;
      int color_sel;

      std::vector<Scenes::SceneValue> scenevalues;
      int scene_sel;

      // parse file
      std::string line;
      while ( !(std::getline(ifile,line).eof()) )
      {
        std::stringstream linestr;
        linestr << line;
        std::vector<std::string> words;
        std::string word;
        bool words_done = false;
        while ( !words_done )
        {
          words_done = std::getline(linestr,word,' ').eof();
          words.push_back(word);
        }
        if ( words.size() )
        {
          if ( words[0] == "color" && words.size() > 4 )
          {
            Settings::ColorValue cv;
            cv.r = (unsigned char)atoi(words[1].c_str());
            cv.g = (unsigned char)atoi(words[2].c_str());
            cv.b = (unsigned char)atoi(words[3].c_str());
            cv.is_primary = (unsigned char)atoi(words[4].c_str());
            colorvalues.push_back(cv);
          }
          else if ( words[0] == "seed" && words.size() > 1 )
          {
            seed = atoi(words[1].c_str());
          }
          else if ( words[0] == "subcards" && words.size() > 1 )
          {
            subcards = atoi(words[1].c_str());
          }
          else if ( words[0] == "color_sel" && words.size() > 1 )
          {
            color_sel = atoi(words[1].c_str());
          }
          else if ( words[0] == "scene" && words.size() > 4 )
          {
            Scenes::SceneValue sv;
            sv.position = atof(words[1].c_str());
            sv.left = atoi(words[2].c_str());
            sv.right = atoi(words[3].c_str());
            sv.duration = atof(words[4].c_str());
            sv.dots.clear();
            for (unsigned int i = 5; i < words.size(); i++)
            {
              sv.dots.push_back(atoi(words[i].c_str()));
            }
            scenevalues.push_back(sv);
          }
          else if ( words[0] == "scene_sel" )
          {
            scene_sel = atoi(words[1].c_str());
          }
        }
      }
      ifile.close();

      settings->set(colorvalues,seed,subcards,color_sel);
      scenes->set(scenevalues,scene_sel);
      preview->CompileSlides();

      is_modified = false;
      unsaved = false;
      settitle();
    }
  }

  void save (const char* fn)
  {
    std::ofstream ofile(fn, std::ios::trunc);
    if ( ofile.is_open() )
    {
      // save settings
      std::vector<Settings::ColorValue>* values = NULL;
      int seed, cards, selection;
      settings->get(values,&seed,&cards,&selection);
      if ( values )
      {
        for (unsigned int i = 0; i < values->size(); i++)
        {
          ofile << "color " << unsigned((*values)[i].r) << " " << unsigned((*values)[i].g)
            << " " << unsigned((*values)[i].b) << " " << unsigned((*values)[i].is_primary) << std::endl;
        }
        ofile << "seed " << seed << std::endl;
        ofile << "subcards " << cards << std::endl;
        ofile << "color_sel " << selection << std::endl;
      }

      // save scenes
      std::vector<Scenes::SceneValue>* svalues = NULL;
      int sselection;
      scenes->get(svalues,&sselection);
      if ( svalues )
      {
        for (unsigned int i = 0; i < svalues->size(); i++)
        {
          ofile << "scene " << (*svalues)[i].position << " " << (*svalues)[i].left
            << " " << (*svalues)[i].right << " " << (*svalues)[i].duration;
          for (unsigned int j = 0; j < (*svalues)[i].dots.size(); j++)
          {
            ofile << " " << (*svalues)[i].dots[j];
          }
          ofile << std::endl;
        }
        ofile << "scene_sel " << sselection << std::endl;
      }

      ofile.close();
      is_modified = false;
      settitle();
    }
  }

  static void cb_new (Fl_Widget* a, void* data)
  {
    ColorBars* cb = (ColorBars*)data;
    if ( cb->is_modified )
    {
      int choice = fl_choice("Do you want to save before starting a new file?","Don't Save","Save","Cancel");
      if ( choice == 1 )
      {
        // save
        cb_save(a,data);
      }
      else if ( choice == 2 )
      {
        // do nothing
        return;
      }
    }

    // new file
    cb->newfile();
  }

  static void cb_save (Fl_Widget* a, void* data)
  {
    ColorBars* cb = (ColorBars*)data;
    if ( cb->is_modified )
    {
      if ( cb->unsaved )
      {
        cb_saveas(a,data);
        cb->unsaved = false;
      }
      else
      {
        cb->save(cb->filename.c_str());
      }
    }
  }

  static void cb_saveas (Fl_Widget* a, void* data)
  {
    ColorBars* cb = (ColorBars*)data;

    Fl_Native_File_Chooser fc;
    fc.title ("Save File As...");
    fc.type(Fl_Native_File_Chooser::BROWSE_SAVE_FILE);
    fc.filter("Text\t*.txt");
    fc.directory(".");
    if ( !fc.show() )
    {
      std::string filename = fc.filename();
      if ( strcmp(fl_filename_ext(fc.filename()),".txt") != 0 )
      {
        filename += ".txt";
      }
      if ( cb->unsaved )
      {
        cb->filename = filename;
      }
      cb->save(filename.c_str());
    }
  }

  static void cb_open (Fl_Widget* a, void* data)
  {
    ColorBars* cb = (ColorBars*)data;
    if ( cb->is_modified )
    {
      int choice = fl_choice("Do you want to save before opening another file?","Don't Save","Save","Cancel");
      if ( choice == 1 )
      {
        // save
        cb_save(a,data);
      }
      else if ( choice == 2 )
      {
        // do nothing
        return;
      }
    }
    // open file
    Fl_Native_File_Chooser fc;
    fc.title ("Open File...");
    fc.type(Fl_Native_File_Chooser::BROWSE_FILE);
    fc.filter("Text\t*.txt");
    fc.directory(".");
    if ( !fc.show() )
    {
      // open file...
      cb->filename = fc.filename();
      cb->open(cb->filename.c_str());
    }
  }

  static void cb_quit (Fl_Widget* a, void* data)
  {
    ColorBars* cb = (ColorBars*)data;
    if ( cb->is_modified )
    {
      int choice = fl_choice("Do you want to save before quitting?","Don't Save","Save","Cancel");
      if ( choice == 1 )
      {
        // save first..
        cb_save(a,data);
      }
      else if ( choice == 2 )
      {
        // do nothing
        return;
      }
    }
    cb->window->hide();
    cb->play_window->hide();
  }

  static void cb_fullscreen (Fl_Widget* a, void* data)
  {
    ColorBars* cb = (ColorBars*)data;
    if ( cb )
    {
      cb->preview->CompileSlides();
      cb->play_window->show();
      cb->window->hide();
    }
  }

  static void cb_tabs (Fl_Widget* a, void* data)
  {
    ColorBars* cb = (ColorBars*)data;
    if ( cb )
    {
      cb->settings->deselect();
      cb->scenes->deselect();
      cb->preview->CompileSlides();
    }
  }

  ColorBars ()
  {
    current = this;

    play_window = new Display(320,240,this);
    play_window->resizable(play_window);
    //play_window->fullscreen();

    play_window_drawing = new Preview::ColorBarDrawing(0,0,play_window->w(),play_window->h());

    window = new Fl_Window(480,320);
    settitle();

    menu = new Fl_Menu_Bar(0,0,480,25);
    menu->textsize(12);
    menu->add("File/New",FL_CTRL+'n',cb_new,this);
    menu->add("File/Save",FL_CTRL+'s',cb_save,this);
    menu->add("File/Save As...\t",FL_CTRL+FL_SHIFT+'s',cb_saveas,this);
    menu->add("File/Open...",FL_CTRL+'o',cb_open,this);
    menu->add("File/Quit",FL_CTRL+'q',cb_quit,this);
    menu->add("Play",FL_CTRL+'f',cb_fullscreen,this);

    tabs = new Fl_Tabs(0,25,480,320);

    settings = new Settings();
    scenes = new Scenes(settings->GetColorValues());
    preview = new Preview(settings->GetColorValues(),scenes->GetSceneValues(),play_window_drawing);
    tabs->end();
    tabs->callback(cb_tabs,this);
    tabs->when(FL_WHEN_RELEASE);

    newfile();

    window->end();
    window->callback(cb_quit,this);

    window->show();
  }

  ~ColorBars ()
  {
    delete settings;
    delete scenes;
    delete preview;
  }
};
ColorBars* ColorBars::current = NULL;

#endif
