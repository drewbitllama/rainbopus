#include "colorbars.h"

void modify ()
{
  if ( ColorBars::current )
  {
    ColorBars::current->modify();
  }
}

int cb_osc (const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
{
  printf("PRINT TIME!\n");
  return 0;
}

class Network
{
#define STARTIP 7770
private:
  ColorBars* app_;
  lo_server_thread st_;
  std::vector<int> servers_;

public:
  Network (ColorBars* app)
    : app_(app)
  {
    int start_ip = STARTIP;

    st_ = 0;
    while ( !st_ )
    {
      char num[32];
      snprintf(num,32,"%d",start_ip);
      st_ = lo_server_thread_new(num,NULL);
      if ( st_ )
      {
        printf("created server @ localhost:%d\n",start_ip);\
        if ( start_ip == STARTIP )
        {
          lo_server_thread_add_method(st_,"/register","ii",cb_new_server,this);
        }

        lo_server_thread_add_method(st_,"/servers","bi",cb_servers,this);
        snprintf(num,32,"%d",STARTIP);
        lo_address la = lo_address_new(NULL,num);
        lo_send(la,"/register","ii",start_ip,1);

        lo_server_thread_add_method(st_,"/play",NULL,cb_play,this);
        lo_server_thread_add_method(st_,"/rewind",NULL,cb_rewind,this);

        lo_server_thread_start(st_);
      }
      else
      {
        start_ip++;
      }
    }
  }

  ~Network ()
  {
    if ( lo_server_thread_get_port(st_) != STARTIP )
    {
      char num[32];
      snprintf(num,32,"%d",STARTIP);
      lo_address la = lo_address_new(NULL,num);
      lo_send(la,"/register","ii",lo_server_thread_get_port(st_),0);
      lo_address_free(la);
    }
    lo_server_thread_stop(st_);
    lo_server_thread_free(st_);
  }

  static int cb_servers (const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
  {
    Network* n = (Network*)user_data;

    lo_blob b = argv[0];
    int c = argv[1]->i;
    int* ports = (int*)lo_blob_dataptr(b);
    printf("current servers: %d\n",c);
    n->servers_.clear();
    for (int i = 0; i < c; i++)
    {
      n->servers_.push_back(ports[i]);
      printf("\t%d\n",ports[i]);
    }
    n->app_->setPorts(n->servers_);
    return 0;
  }

  static int cb_new_server (const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
  {
    Network* n = (Network*)user_data;
    if ( argv[1]->i )
    {
      printf("server added @ localhost:%d\n",argv[0]->i);
      n->servers_.push_back(argv[0]->i);

      printf("sending these servers: ");
      for (unsigned int i = 0; i < n->servers_.size(); i++)
      {
        printf(":%d, ",n->servers_[i]);
      }
      printf("\n");
      n->app_->setPorts(n->servers_);

      // update all registered clients...
      //
      lo_blob b = lo_blob_new(sizeof(int)*n->servers_.size(),&n->servers_[0]);
      for (unsigned int i = 0; i < n->servers_.size(); i++)
      {
        char num[32];
        snprintf(num,32,"%d",n->servers_[i]);
        lo_address t = lo_address_new(NULL,num);
        lo_send(t,"/servers","bi",b,n->servers_.size());
        lo_address_free(t);
      }
      lo_blob_free(b);
    }
    else
    {
      printf("server removed @ localhost:%d\n",argv[0]->i);
      for (unsigned int i = 0; i < n->servers_.size(); i++)
      {
        if ( n->servers_[i] == argv[0]->i )
        {
          n->servers_.erase(n->servers_.begin()+i);
          break;
        }
      }
      n->app_->setPorts(n->servers_);

      // update all registered clients...
      //
      lo_blob b = lo_blob_new(sizeof(n->servers_),&n->servers_[0]);
      for (unsigned int i = 0; i < n->servers_.size(); i++)
      {
        char num[32];
        snprintf(num,32,"%d",n->servers_[i]);
        lo_address t = lo_address_new(NULL,num);
        lo_send(t,"/servers","bi",b,n->servers_.size());
        lo_address_free(t);
      }
      lo_blob_free(b);
    }
    return 0;
  }

  static int cb_play (const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
  {
    Network* n = (Network*)user_data;
    Fl::awake(n->app_->preview->cb_playnet,n->app_->preview);
    return 0;
  }

  static int cb_rewind (const char *path, const char *types, lo_arg **argv, int argc, void *data, void *user_data)
  {
    Network* n = (Network*)user_data;
    Fl::awake(n->app_->preview->cb_rewind,n->app_->preview);
    return 0;
  }
};

int main (int argc, char* argv[])
{

  ColorBars* colorbars = new ColorBars;
  Network net(colorbars);

  Fl::lock();
  Fl::run();

  delete colorbars;

  return 0;
}
