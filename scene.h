#ifndef SCENE_H__
#define SCENE_H__

#include "main.h"
#include "settings.h"

enum
{
  DOT_NONE = 0,
  DOT_FORWARD,
  DOT_BACK,
};

class Scenes
{
public:
  struct SceneValue
  {
    float position;
    int left;
    int right;
    float duration;
    std::vector<int> dots;

    SceneValue ()
    : position(0),left(0),right(0),duration(1.0f)
    {
      dots.push_back(0);
    }
  };

private:
  Fl_Group* group;
  Fl_Hold_Browser* browser;
  Fl_Button* add;
  Fl_Button* del;
  Fl_Button* up;
  Fl_Button* down;

  Fl_Group* single_scene;
  Fl_Slider* position;
  Fl_Int_Input* left;
  Fl_Int_Input* right;
  Fl_Float_Input* duration;
  Fl_Scroll* dotscroll;
  Fl_Box* dotlabels[3];

  std::vector<SceneValue> scenevalues;
  std::vector<Settings::ColorValue>& colorvalues;

public:
  static Scenes* current;

  static void add_scene (Fl_Widget* a, void* data)
  {
    modify();

    Scenes* s = (Scenes*)data;
    s->browser->add("scene");
    s->scenevalues.push_back(SceneValue());
  }

  static void del_scene (Fl_Widget* a, void* data)
  {
    modify();

    Scenes* s = (Scenes*)data;
    int v = s->browser->value();
    if ( v )
    {
      s->browser->remove(v);
      s->scenevalues.erase(s->scenevalues.begin()+(v-1));
      s->single_scene->deactivate();
    }
  }

  static void up_scene (Fl_Widget* a, void* data)
  {
    modify();

    Scenes* s = (Scenes*)data;
    int v = s->browser->value();
    if ( v > 1 )
    {
      std::iter_swap(s->scenevalues.begin()+(v-1),s->scenevalues.begin()+(v-2));
      s->browser->swap(v,v-1);
      s->browser->select(v,0);
      s->browser->value(v-1);
    }
  }

  static void down_scene (Fl_Widget* a, void* data)
  {
    modify();

    Scenes* s = (Scenes*)data;
    int v = s->browser->value();
    if ( v && v < s->browser->size() )
    {
      std::iter_swap(s->scenevalues.begin()+(v-1),s->scenevalues.begin()+(v));
      s->browser->swap(v,v+1);
      s->browser->select(v,0);
      s->browser->value(v+1);
    }
  }

  static void sel_scene (Fl_Widget* a, void* data)
  {
    Scenes* s = (Scenes*)data;
    int v = s->browser->value();
    if ( v )
    {
      SceneValue& sv = s->scenevalues[v-1];
      char num[10];

      s->position->value(sv.position);

      snprintf(num,10,"%d",sv.left);
      s->left->value(num);

      snprintf(num,10,"%d",sv.right);
      s->right->value(num);

      snprintf(num,10,"%f",sv.duration);
      s->duration->value(num);

      s->display_dots_gui(sv);

      s->single_scene->activate();
    }
    else
    {
      s->single_scene->deactivate();
    }
  }

  static void change_position (Fl_Widget* a, void* data)
  {
    modify();

    Scenes* s = (Scenes*)data;
    int v = s->browser->value();
    if ( v )
    {
      SceneValue& sv = s->scenevalues[v-1];
      sv.position = s->position->value();
      s->display_dots_gui(sv);
    }
  }

  static void change_left (Fl_Widget* a, void* data)
  {
    modify();

    Scenes* s = (Scenes*)data;
    int v = s->browser->value();
    if ( v )
    {
      SceneValue& sv = s->scenevalues[v-1];
      sv.left = atoi(s->left->value());
      s->display_dots_gui(sv);
    }
  }

  static void change_right (Fl_Widget* a, void* data)
  {
    modify();

    Scenes* s = (Scenes*)data;
    int v = s->browser->value();
    if ( v )
    {
      SceneValue& sv = s->scenevalues[v-1];
      sv.right = atoi(s->right->value());
      s->display_dots_gui(sv);
    }
  }

  static void change_duration (Fl_Widget* a, void* data)
  {
    modify();

    Scenes* s = (Scenes*)data;
    int v = s->browser->value();
    if ( v )
    {
      s->scenevalues[v-1].duration = atof(s->duration->value());
    }
  }

  static void cb_radio (Fl_Widget* a, void* data)
  {
    modify();

    Fl_Group* p = a->parent();
    if ( !p ) return;

    Fl_Group* pp = p->parent();
    if ( !pp ) return;

    Scenes* s = (Scenes*)data;
    if ( !s ) return;

    int v = s->browser->value();
    if ( v )
    {
      int col = -1;
      for (int i = 0; i < pp->children(); i++)
      {
        if ( pp->child(i) == p )
        {
          col = i;
          break;
        }
      }
      if ( col < 0 ) return;

      int row = -1;
      for (int j = 0; j < p->children(); j++)
      {
        row++;
        if ( p->child(j)->type() == FL_RADIO_BUTTON )
        {
          Fl_Round_Button* radio = (Fl_Round_Button*)p->child(j);
          if ( radio->value() )
          {
            break;
          }
        }
      }
      if ( row < 0 ) return;

      printf("%d %d\n",col,row);

      SceneValue& sv = s->scenevalues[v-1];
      sv.dots[col] = row - 1;
    }
  }

  void display_dots_gui (SceneValue& sv)
  {
    int cs, s, c, l, r;

    cs = int(colorvalues.size()) - 1;
    if ( cs < 0 ) return;

    c = int(sv.position * float(cs+1));
    c = std::min(std::max(0,c),cs);

    l = c - sv.left;
    l = std::min(std::max(0,l),cs);

    r = c + sv.right;
    r = std::min(std::max(0,r),cs);

    s = (r - l) + 1;
    s = std::min(std::max(0,s),cs+1);
    if ( s < 1 ) return;

    sv.dots.resize(s);
    dotscroll->clear();
    dotscroll->begin();

    for (unsigned int i = 0; i < sv.dots.size(); i++)
    {
      Fl_Pack* pack = new Fl_Pack(200+(i*30),210,30,80);
      pack->type(Fl_Pack::VERTICAL);
      pack->box(FL_UP_FRAME);
      pack->begin();

      Fl_Box* color = new Fl_Box(0,0,20,20);
      color->box(FL_FLAT_BOX);
      color->type(0);
      Settings::ColorValue& cv = colorvalues[l+i];
      color->color(fl_rgb_color(cv.r,cv.g,cv.b));

      for (int j = 0; j < 3; j++)
      {
        Fl_Radio_Round_Button* radio = new Fl_Radio_Round_Button(0,0,20,20);
        radio->type(FL_RADIO_BUTTON);
        if ( sv.dots[i] == j ) radio->setonly();
        radio->callback(cb_radio,this);
      }

      pack->end();
      pack->redraw();
    }

    dotscroll->end();
    dotscroll->redraw();
  }

  void set (const std::vector<Scenes::SceneValue>& values, int selection)
  {
    browser->clear();
    single_scene->deactivate();

    scenevalues = values;
    for (unsigned int i = 0; i < scenevalues.size(); i++)
    {
      browser->add("scene");
    }
    browser->value(selection);
    sel_scene(NULL,this);
  }

  void get (std::vector<Scenes::SceneValue>*& values, int* selection)
  {
    values = &scenevalues;
    if ( selection ) *selection = browser->value();
  }

  Scenes (std::vector<Settings::ColorValue>& cvs)
   : colorvalues(cvs)
  {
    Scenes::current = this;

    group = new Fl_Group(0, 50, 480, 295);
    group->label("Scenes");
    group->labelsize(12);

    browser = new Fl_Hold_Browser(10, 60, 120, 225);
    browser->labelsize(12);
    browser->callback(sel_scene,this);

    add = new Fl_Button(10, 285, 30, 25, "+");
    add->callback(add_scene,this);

    del = new Fl_Button(40, 285, 30, 25, "-");
    del->callback(del_scene,this);

    up = new Fl_Button(70, 285, 30, 25, "up");
    up->labelsize(10);
    up->callback(up_scene,this);

    down = new Fl_Button(100, 285, 30, 25, "down");
    down->labelsize(10);
    down->callback(down_scene,this);

    single_scene = new Fl_Group(140,60,330,295);

    position = new Fl_Slider(200,60,270,25,"Position");
    position->align(FL_ALIGN_LEFT);
    position->type(FL_HORIZONTAL);
    position->labelsize(12);
    position->callback(change_position,this);
    position->when(FL_WHEN_CHANGED);

    left = new Fl_Int_Input(200,90,270,25,"Left");
    left->labelsize(12);
    left->value("0");
    left->callback(change_left,this);
    left->when(FL_WHEN_CHANGED);

    right = new Fl_Int_Input(200,120,270,25,"Right");
    right->labelsize(12);
    right->value("0");
    right->callback(change_right,this);
    right->when(FL_WHEN_CHANGED);

    duration = new Fl_Float_Input(200,150,270,25,"Duration");
    duration->labelsize(12);
    duration->value("0");
    duration->callback(change_duration,this);
    duration->when(FL_WHEN_CHANGED);

    dotlabels[0] = new Fl_Box(200,230,20,20,"none");
    dotlabels[0]->align(FL_ALIGN_LEFT);
    dotlabels[0]->labelsize(12);

    dotlabels[1] = new Fl_Box(200,250,20,20,"forward");
    dotlabels[1]->align(FL_ALIGN_LEFT);
    dotlabels[1]->labelsize(12);

    dotlabels[2] = new Fl_Box(200,270,20,20,"backward");
    dotlabels[2]->align(FL_ALIGN_LEFT);
    dotlabels[2]->labelsize(12);

    dotscroll = new Fl_Scroll(200,210,270,100,"Card Symbols:");
    dotscroll->labelsize(12);
    dotscroll->align(FL_ALIGN_TOP_LEFT);

    SceneValue sv;
    sv.dots.resize(8);
    display_dots_gui(sv);

    single_scene->end();
    single_scene->deactivate();

    group->end();
  }

  void deselect ()
  {
    browser->select(browser->value(),0);
    single_scene->deactivate();
  }

  std::vector<SceneValue>& GetSceneValues () { return scenevalues; }
};
Scenes* Scenes::current = NULL;

#endif
