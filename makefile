CC=g++
CFLAGS=-c -Wall -I/Users/dreyas/Downloads/fltk-1.3.2/ -I/Users/dreyas/Downloads/liblo-0.26
LDFLAGS=-L/Users/dreyas/Downloads/fltk-1.3.2/lib -L/Users/dreyas/Downloads/liblo-0.26/
LIBS=-lfltk -llo_s -lpthread -framework Cocoa
SOURCES=main.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=rainbopus

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(LIBS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf *.o $(EXECUTABLE)