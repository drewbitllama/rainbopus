#ifndef SETTINGS_H__
#define SETTINGS_H__

#include "main.h"

class Settings
{
public:
  struct ColorValue
  {
    unsigned char r,g,b,is_primary;
    char str[8];

    ColorValue (unsigned char r = 255, unsigned char g = 255, unsigned char b = 255, unsigned char is_primary = 1)
    : r(r),g(g),b(b),is_primary(is_primary) {}

    const char* hex ()
    {
      std::stringstream ss;
      ss << std::setfill('0') << "#" << std::hex;
      ss << std::setw(2) << unsigned(r);
      ss << std::setw(2) << unsigned(g);
      ss << std::setw(2) << unsigned(b) << '\0';
      snprintf(str,8,ss.str().c_str());
      return str;
    }
  };

private:
  Fl_Group* group;
  Fl_Hold_Browser* browser;
  Fl_Button* add;
  Fl_Button* del;
  Fl_Button* up;
  Fl_Button* down;
  Fl_Color_Chooser* color_selector;
  Fl_Check_Button* is_primary;
  Fl_Int_Input* rndseed;
  Fl_Int_Input* subcards;

  std::vector<ColorValue> colorvalues;

public:
  static Settings* current;

  static void add_color (Fl_Widget* a, void* data)
  {
    modify();

    Settings* s = (Settings*)data;
    ColorValue cv;
    s->browser->add(cv.hex());
    s->colorvalues.push_back(cv);
  }

  static void del_color (Fl_Widget* a, void* data)
  {
    modify();

    Settings* s = (Settings*)data;
    int v = s->browser->value();
    if ( v )
    {
      s->browser->remove(v);
      s->colorvalues.erase(s->colorvalues.begin()+(v-1));
      s->color_selector->deactivate();
      s->is_primary->deactivate();
    }
  }

  static void up_color (Fl_Widget* a, void* data)
  {
    modify();

    Settings* s = (Settings*)data;
    int v = s->browser->value();
    if ( v > 1 )
    {
      std::iter_swap(s->colorvalues.begin()+(v-1),s->colorvalues.begin()+(v-2));
      s->browser->swap(v,v-1);
      s->browser->select(v,0);
      s->browser->value(v-1);
    }
  }

  static void down_color (Fl_Widget* a, void* data)
  {
    modify();

    Settings* s = (Settings*)data;
    int v = s->browser->value();
    if ( v && v < s->browser->size() )
    {
      std::iter_swap(s->colorvalues.begin()+(v-1),s->colorvalues.begin()+(v));
      s->browser->swap(v,v+1);
      s->browser->select(v,0);
      s->browser->value(v+1);
    }
  }

  static void sel_color (Fl_Widget* a, void* data)
  {
    Settings* s = (Settings*)data;
    int v = s->browser->value();
    if ( v )
    {
      double r, g, b;
      r = double(s->colorvalues[v-1].r)/255.0;
      g = double(s->colorvalues[v-1].g)/255.0;
      b = double(s->colorvalues[v-1].b)/255.0;
      s->color_selector->rgb(r,g,b);
      s->color_selector->activate();

      s->is_primary->value(s->colorvalues[v-1].is_primary);
      s->is_primary->activate();
    }
    else
    {
      s->color_selector->deactivate();
      s->is_primary->deactivate();
    }
  }

  static void change_color (Fl_Widget* a, void* data)
  {
    modify();

    Settings* s = (Settings*)data;
    Fl_Color_Chooser* cc = (Fl_Color_Chooser*)a;
    int v = s->browser->value();
    if ( v )
    {
      s->colorvalues[v-1].r = char(cc->r() * 255.0);
      s->colorvalues[v-1].g = char(cc->g() * 255.0);
      s->colorvalues[v-1].b = char(cc->b() * 255.0);
      s->browser->text(v,s->colorvalues[v-1].hex());
    }
  }

  static void change_is_primary (Fl_Widget* a, void* data)
  {
    modify();

    Settings* s = (Settings*)data;
    Fl_Check_Button* cb = (Fl_Check_Button*)a;
    int v = s->browser->value();
    if ( v )
    {
      s->colorvalues[v-1].is_primary = cb->value();
    }
  }

  static void change_seed (Fl_Widget* a, void* data)
  {
    modify();

    Fl_Int_Input* ii = (Fl_Int_Input*)a;
    int v = atoi(ii->value());
    char num[32];
    if ( v < 0 ) v = 0;
    snprintf(num,32,"%d",v);
    ii->value(num);
  }

  static void change_subcards (Fl_Widget* a, void* data)
  {
    modify();

    Fl_Int_Input* ii = (Fl_Int_Input*)a;
    int v = atoi(ii->value());
    char num[32];
    if ( v < 1 ) v = 1;
    snprintf(num,32,"%d",v);
    ii->value(num);
  }

  void set (std::vector<Settings::ColorValue> const& values, int seed, int cards, int selection)
  {
    browser->clear();
    color_selector->deactivate();
    is_primary->deactivate();

    colorvalues = values;
    for (unsigned int i = 0; i < colorvalues.size(); i++)
    {
      browser->add(colorvalues[i].hex());
    }
    char num[32];
    snprintf(num,32,"%d",seed);
    rndseed->value(num);
    snprintf(num,32,"%d",cards);
    subcards->value(num);

    browser->value(selection);
    sel_color(NULL,this);
  }

  void get (std::vector<Settings::ColorValue>*& values, int* seed, int* cards, int* selection)
  {
    values = &colorvalues;
    if ( seed ) *seed = atoi(rndseed->value());
    if ( cards ) *cards = atoi(subcards->value());
    if ( selection ) *selection = browser->value();
  }

  Settings ()
  {
    Settings::current = this;

    group = new Fl_Group(0, 50, 480, 295);
    group->label("Settings");
    group->labelsize(12);

    browser = new Fl_Hold_Browser(10, 60, 120, 225);
    browser->labelsize(12);
    browser->callback(sel_color,this);

    add = new Fl_Button(10, 285, 30, 25, "+");
    add->callback(add_color,this);

    del = new Fl_Button(40, 285, 30, 25, "-");
    del->callback(del_color,this);

    up = new Fl_Button(70, 285, 30, 25, "up");
    up->labelsize(10);
    up->callback(up_color,this);

    down = new Fl_Button(100, 285, 30, 25, "down");
    down->labelsize(10);
    down->callback(down_color,this);

    color_selector = new Fl_Color_Chooser(140,60,330,180);
    color_selector->callback(change_color,this);
    color_selector->deactivate();

    is_primary = new Fl_Check_Button(140,240,120,25," Is Primary?");
    is_primary->labelsize(12);
    is_primary->callback(change_is_primary,this);
    is_primary->deactivate();

    rndseed = new Fl_Int_Input(350,255,120,25,"Random Seed");
    rndseed->labelsize(12);
    rndseed->value("0");
    rndseed->callback(change_seed,this);
    rndseed->when(FL_WHEN_CHANGED);

    subcards = new Fl_Int_Input(350,285,120,25,"No. of Subcards");
    subcards->labelsize(12);
    subcards->value("1");
    subcards->callback(change_subcards,this);
    subcards->when(FL_WHEN_CHANGED);

    group->end();
  }

  void deselect ()
  {
    browser->select(browser->value(),0);
    color_selector->deactivate();
    is_primary->deactivate();
  }

  std::vector<ColorValue>& GetColorValues () { return colorvalues; }
};
Settings* Settings::current = NULL;

#endif
